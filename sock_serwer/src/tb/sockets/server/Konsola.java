package tb.sockets.server;

import javax.swing.JFrame;

public class Konsola {

	public static void main(String[] args) {
		MainFrame frame = new MainFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.startServer();
	}

}
