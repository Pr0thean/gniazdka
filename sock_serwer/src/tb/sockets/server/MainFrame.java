package tb.sockets.server;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class MainFrame extends JFrame {

	private JTextField userText;
	private JTextArea chatWindow;
	private ObjectOutputStream output;	//wysyla tekst
	private ObjectInputStream input;	//przyjmuje tekst
	private ServerSocket serverSock;	//stawia serwer
	private Socket connection;		//tworzy polaczenie miedzy serwerem a klientem
	private JPanel contentPane, panel2;
	
	public MainFrame() {
		super("Komunikator serwer");
		setBounds(100, 100, 650, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblHost = new JLabel("Serwer");
		lblHost.setBounds(10, 14, 50, 20);
		contentPane.add(lblHost);
		
		panel2 = new JPanel();
		panel2.setBounds(145, 14, 487, 448);
		
		userText = new JTextField(30);
		userText.setEditable(false);
		userText.addActionListener(
				new ActionListener() {	
					public void actionPerformed(ActionEvent e) {
						sendMessage(e.getActionCommand());
						userText.setText("");	//zeby po wyslaniu tekst nie zostawal u wysylajacego
					}
				});
		panel2.add(userText, BorderLayout.NORTH);
		
		chatWindow = new JTextArea(20,30);
		chatWindow.setEditable(false);
		panel2.add(new JScrollPane(chatWindow), BorderLayout.CENTER);
		contentPane.add(panel2);
		setVisible(true);
	}
	
	public void startServer() {
		try {
			serverSock = new ServerSocket(22222);
			while(true) {
				try {
					waitForConnection();
					setupStreams(); 
					whileChatting();
				} catch(EOFException eof) {
					showMessage("Zakonczono polaczenie");
				} finally {
					closeStreams();
				}
			}
		} catch (IOException io) {
			io.printStackTrace();
		}
	}
	
	private void waitForConnection() throws IOException {
		showMessage("Czekam na polaczenie \n");
		connection = serverSock.accept();	//akceptuje polaczenie
		showMessage("Polaczono z " + connection.getInetAddress().getHostAddress());
	}
	//pobiera strumien do wysylanai i otrzymywania danych
	private void setupStreams() throws IOException {
		output = new ObjectOutputStream(connection.getOutputStream());
		output.flush();		//upewnienie sie ze wszystko zostalo wyslane
		input = new ObjectInputStream(connection.getInputStream());
		showMessage("\n Strumienie zostaly ustalone \n");
	}
	//ta metoda bedzie dzialac podczas chatu
	private void whileChatting() throws IOException {
		String message = "Jestes polaczony";
		sendMessage(message);
		ableToWrite(true);	//pozwala pisac
		do {
			try {
				message = (String) input.readObject();	//odczytuje wiadomosci
				showMessage("\n" + message);
			} catch(ClassNotFoundException cNF) {
				showMessage("\n wyslano cos dziwnego\n");
			}
			
		} while(!message.equals("CLIENT: END")); //konczy rozmowe
	}
	//zamyka wszystko
	private void closeStreams() {
		showMessage("\n Zamykanie polaczen\n");
		ableToWrite(false);	//zamyka mozliwosc pisania
		try {
			output.close();
			input.close();
			connection.close();
		} catch(IOException io) {
			io.printStackTrace();
		}
	}
	// wysyla wiadomosc do klienta
	private void sendMessage(String message) {
		try {
			output.writeObject("SERVER: " + message);	//wysyla wiadomosc
			output.flush();		//i wszystko co sie nie wyslalo
			showMessage("\nSERVER: " + message);	//pokazuje na naszym ekranie
		} catch(IOException io) {
			chatWindow.append("\n Nie mozna wyslac wiadomosc");
		}
	}
	// pokazuje tekst na ekranie
	private void showMessage(final String text) {
		//updata'uje gui
		SwingUtilities.invokeLater(
			new Runnable() {
				public void run() {
					chatWindow.append(text);	
				}
			}
		);
	}
	// pozwala uzytkownikowi pisac
	private void ableToWrite(final boolean letWrite) {
		//updata'uje gui
		SwingUtilities.invokeLater(
			new Runnable() {
				public void run() {
					userText.setEditable(letWrite);	
				}
			}
		);
	}

}
