package tb.sockets.client;

import java.text.ParseException;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.text.MaskFormatter;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JFormattedTextField;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;

public class MainFrame extends JFrame {

	private JTextField userText;
	private JTextArea chatWindow;
	private ObjectOutputStream output;
	private ObjectInputStream input;
	private String message = "";
	private String serverIP;
	private Socket connection;
	private JPanel contentPane, panel2;
	private JFormattedTextField frmtdtxtfldIp;
	private JFormattedTextField frmtdtxtfldXxxx;
	
	public MainFrame() {
		super("Klient");
		
		setBounds(100, 100, 650, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblHost = new JLabel("Host:");
		lblHost.setBounds(10, 14, 30, 20);
		contentPane.add(lblHost);
		
		try {
			frmtdtxtfldIp = new JFormattedTextField(new MaskFormatter("###.#.#.#"));
			frmtdtxtfldIp.setBounds(43, 11, 90, 20);
			frmtdtxtfldIp.setText("xxx.x.x.x");
			contentPane.add(frmtdtxtfldIp);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		frmtdtxtfldXxxx = new JFormattedTextField();
		frmtdtxtfldXxxx.setText("xxxxx");
		frmtdtxtfldXxxx.setBounds(43, 39, 90, 20);
		contentPane.add(frmtdtxtfldXxxx);
		
		JLabel lblPort = new JLabel("Port:");
		lblPort.setBounds(10, 42, 30, 20);
		contentPane.add(lblPort);
		
		JLabel lblNotConnected = new JLabel("Not Connected");
		lblNotConnected.setForeground(new Color(255, 255, 255));
		lblNotConnected.setBackground(new Color(128, 128, 128));
		lblNotConnected.setOpaque(true);
		lblNotConnected.setBounds(10, 104, 123, 23);
		contentPane.add(lblNotConnected);

		JButton btnConnect = new JButton("Connect");
		btnConnect.setBounds(10, 70, 100, 23);
		btnConnect.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent e) {
				new Thread() {
					public void run() {
						if(frmtdtxtfldIp.getText().equals("127.0.0.1") && frmtdtxtfldXxxx.getText().equals("22222")) {
							lblNotConnected.setText("Connected");
							btnConnect.setEnabled(false);
							startClient();
						}
					}
				}.start();
			}	
		});
		contentPane.add(btnConnect);
		
		panel2 = new JPanel();
		panel2.setBounds(145, 14, 487, 448);
		
		userText = new JTextField(30);
		userText.setEditable(false);
		userText.addActionListener(
				new ActionListener() {	
					public void actionPerformed(ActionEvent e) {
						sendMessage(e.getActionCommand());
						userText.setText("");
					}
				});
		panel2.add(userText, BorderLayout.NORTH);
		
		chatWindow = new JTextArea(20,30);
		chatWindow.setEditable(false);
		panel2.add(new JScrollPane(chatWindow), BorderLayout.CENTER);
		contentPane.add(panel2);
		setVisible(true);
	}
	
	//łączenie z serwerem
	public void startClient() {
		try {
			connectToServer();
			setupStreams();
			whileChatting();
		} catch(EOFException EOF) {
			showMessage("\nKlient zakonczyl polaczenie");
		} catch(IOException io) {
			io.printStackTrace();
		} finally {
			closeStreams();
		}
	}
	
	// łączenie z serwerem
	private void connectToServer() throws IOException {
		showMessage("Szukam polaczenia\n");
		connection = new Socket(InetAddress.getByName(serverIP),22222);
		showMessage("Polaczono z serwerem " + connection.getInetAddress().getHostName());
	}
	
	// ustawia polaczenia do wymainy wiadomosc
	private void setupStreams() throws IOException {
		output = new ObjectOutputStream(connection.getOutputStream());
		output.flush();
		input = new ObjectInputStream(connection.getInputStream());
		showMessage("\n Strumienie zostaly ustalone \n");
	}
	
	private void whileChatting() throws IOException {
		ableToWrite(true);
		do {
			try {
				message = (String) input.readObject();	//odczytuje wiadomosci
				showMessage("\n" + message);
			} catch(ClassNotFoundException cNF) {
				showMessage("\n wyslano cos dziwnego\n");
			}
		} while(!message.equals("SERVER: END"));
	}
	
	//zamyka strumienei i gniazdka
	private void closeStreams() {
		showMessage("\n Zamykanie polaczen\n");
		ableToWrite(false);	//zamyka mozliwosc pisania
		try {
			output.close();
			input.close();
			connection.close();
		} catch(IOException io) {
			io.printStackTrace();
		}
	}
	
	//wysylanie wiadomosc
	private void sendMessage(String message) {
		try {
			output.writeObject("CLIENT: " + message);	//wysyla wiadomosc
			output.flush();		//i wszystko co sie nie wyslalo
			showMessage("\nCLIENT: " + message);	//pokazuje na naszym ekranie
		} catch(IOException io) {
			chatWindow.append("\n Nie mozna wyslac wiadomosc");
		}
	}
	// zmienia/updatuje chatWindow
	private void showMessage(final String text) {
		SwingUtilities.invokeLater(
			new Runnable() {
				public void run() {
					chatWindow.append(text);
				}
			}
		);
	}
	
	// pozwolenie na pisanie
	private void ableToWrite(final boolean letWrite) {
		SwingUtilities.invokeLater(
			new Runnable() {
				public void run() {
					userText.setEditable(letWrite);
				}
			}
		);
	}
}
